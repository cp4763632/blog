#include <iostream>
#include <memory>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <vector>

using delete_fn = void(void*);

template <typename T>
static void delete_help(void* ptr) {
    if (ptr)
        delete static_cast<T*>(ptr);
}

class Any {
    struct __non_existing_any {};

  public:
    Any()
        : type_info_(typeid(__non_existing_any)) {
    }

    template <typename T>
    Any(T&& value)
        : type_info_(typeid(T)) {
        delete_helper_ = &delete_help<T>;
        ptr_           = new T(std::forward<T>(value));
    }

    Any(Any&& other)
        : type_info_{other.type_info_}
        , delete_helper_{other.delete_helper_}
        , ptr_{other.ptr_} {

        other.erase_state();
    }

    Any& operator=(Any&& other) {
        delete_state();

        type_info_     = other.type_info_;
        delete_helper_ = other.delete_helper_;
        ptr_           = other.ptr_;

        other.erase_state();

        return *this;
    }

    ~Any() {
        delete_state();
    }

    template <typename T>
    T& value() {
        if (!ptr_)
            throw std::runtime_error("no object contained");
        if (type_info_ != typeid(T))
            throw std::runtime_error("bad any cast");
        return *(static_cast<T*>(ptr_));
    }

    const std::type_index& type() const {
        return type_info_;
    }

  private:
    void erase_state() {
        ptr_           = nullptr;
        delete_helper_ = nullptr;
        type_info_     = typeid(__non_existing_any);
    }

    void delete_state() {
        if (delete_helper_) {
            delete_helper_(ptr_);
            ptr_ = nullptr;
        }
    }

  private:
    std::type_index type_info_;
    delete_fn*      delete_helper_{nullptr};
    void*           ptr_ = nullptr;
};

void print(Any& any) {
    if (any.type() == std::type_index{typeid(int)}) {
        std::cout << any.value<int>() << std::endl;
    } else if (any.type() == std::type_index{typeid(std::vector<int>)}) {
        for (int x : any.value<std::vector<int>>())
            std::cout << x << " ";
        std::cout << std::endl;
    } else if (any.type() == std::type_index{typeid(std::string)}) {
        std::cout << any.value<std::string>() << std::endl;
    } else {
        std::cout << "cant handle type" << std::endl;
    }
}

// Compile: g++ -std=c++20 -Wall any.cpp -o any
int main() {
    Any a{int{3}};
    print(a);

    Any b{std::vector<int>{1, 3, 4, 5, 6}};
    print(b);

    Any c{std::string{"awesome string"}};
    print(c);

    try {
        c.value<double>();
    } catch (const std::exception& e) {
        std::cerr << "exception: " << e.what() << std::endl;
    }

    Any d{int{2}};
    d = std::move(c);
    print(d);
}
